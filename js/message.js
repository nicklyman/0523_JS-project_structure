exports.Message = function(to, from, subject, messageText) {
  this.to = to;
  this.from = from;
  this.subject = subject;
  this.messageText = messageText;
  // var displayedMessage = "";
}

exports.Message.prototype.read = function() {
  this.displayedMessage = "Dear " + this.to + ", regarding " + this.subject + ", " + this.messageText + " Yours truly, " + this.from;
  return this.displayedMessage;
}

exports.Message.prototype.reverse = function() {
  var messageArray = [];
  messageArray = this.displayedMessage.split("");
  messageArray.reverse();
  this.displayedMessage = messageArray.join("");
  return this.displayedMessage;
}
