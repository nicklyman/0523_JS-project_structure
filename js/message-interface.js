var Message = require('./../js/message.js').Message;

$(document).ready(function(){
  $('#email').submit(function(event){
    event.preventDefault();
    var toField = $('#to').val();
    var fromField = $('#from').val();
    var subjectField = $('#subject').val();
    var textField = $('#message').val();
    var newMessage = new Message(toField,fromField,subjectField,textField);
    newMessage.read();
    $('#output').append("<li>" + newMessage.reverse() + "</li>");
  });
});
